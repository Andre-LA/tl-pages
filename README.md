# tlpages

Teal library to generate html

## Example

```teal
local Element = require 'tlpages.element'
local HTMTable = require 'tlpages.htmtable'
local Page = require 'tlpages.page'

local function head(title: string): HTMTable.ElementTable
	return {'head', nil, {
		{'meta', {charset = 'utf-8'}},
		{'title', nil, title}
	}}
end

local function page(title: string, content: {HTMTable.ElementTable}): {Element}
	return HTMTable.new {
		{'html', nil, {
			head(title),
			{'body', nil, content},
		}}
	}
end

local function hello_world(): {Element}
	return page('Hello, world!', {
		{'h1', nil, 'Hello, World!'},
		{'p', nil, 'Olá, mundo!'}
	}) 
end

assert(Page.elements_to_page(hello_world(), "out/hello_world.html"))
```
