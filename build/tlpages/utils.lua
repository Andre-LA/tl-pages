local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local pairs = _tl_compat and _tl_compat.pairs or pairs; local table = _tl_compat and _tl_compat.table or table; local Utils = {}


function Utils.imap(t, fn)
   local result = {}
   for i, v in ipairs(t) do
      result[i] = fn(v)
   end
   return result
end

function Utils.pairs_to_list(t, fn)
   local result = {}
   for k, v in pairs(t) do
      table.insert(result, fn(k, v))
   end
   return result
end

return Utils
