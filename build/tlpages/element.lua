local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table; local type = type




local Utils = require('tlpages.utils')
local Tag = require('tlpages.tag')

local Element = {}





local Element_mt = { __index = Element }

function Element.new(tag, attributes, ...)
   return setmetatable({
      tag = tag,
      attributes = attributes,
      contents = { ... },
   }, Element_mt)
end

function Element:render(level)
   level = level or 0
   local has_tree = false

   local contents = Utils.imap(
   self.contents or {},
   function(content)
      local is_tree = type(content) == "table"
      if is_tree then
         has_tree = true
      end
      return (
      is_tree and
      (content):render(level + 1) or
      (content))

   end)


   local separator = ''

   if has_tree then
      local indent0 = string.rep('\t', level)
      local indent1 = string.rep('\t', level + 1)

      table.insert(contents, 1, '')

      separator = table.concat(contents, has_tree and '\n' .. indent1 or '') .. ('\n' .. indent0)
   else
      separator = table.concat(contents)
   end

   return Tag.render(self.tag, self.attributes, separator)
end

return Element
