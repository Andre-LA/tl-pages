local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local io = _tl_compat and _tl_compat.io or io; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table




local Utils = require('tlpages.utils')
local Element = require('tlpages.element')

local Page = {}


function Page.doctype(doctype)
   return Element.new('!DOCTYPE', { doctype or 'html' }, nil)
end

function Page.elements_to_page(elements, output_file, doctype)
   table.insert(elements, 1, Page.doctype(doctype))

   local content = table.concat(Utils.imap(
   elements,
   function(e) return e:render() end),
   '\n')

   local output = io.open(output_file, 'w+')

   if not output then
      return false, string.format("file '%s' couldn't be found", output_file)
   end

   output:write(content)
   output:close()

   return true, ""
end

return Page
