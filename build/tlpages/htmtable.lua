local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table; local _tl_table_unpack = unpack or table.unpack; local type = type




local Utils = require('tlpages.utils')
local Element = require('tlpages.element')
local Tag = require('tlpages.tag')

local HTMTable = {}



local function extract_element(element)
   local tag = element[1]
   local attributes_table = element[2]
   local content = element[3]

   local attributes = nil


   if attributes_table then

      attributes = Utils.pairs_to_list(
      attributes_table,
      function(name, value)
         return { name, value }
      end)



      table.sort(attributes, function(a, b)
         local a_attrib, b_attrib = '', ''

         if type(a) == "string" then
            a_attrib = a
         else
            a_attrib = a[1]
         end
         if type(b) == "string" then
            b_attrib = b
         else
            b_attrib = b[1]
         end

         return string.byte(a_attrib) < string.byte(b_attrib)
      end)
   end


   if type(content) == "string" then
      return Element.new(tag, attributes, content)
   end


   if type(content) == 'nil' then
      return Element.new(tag, attributes)
   end


   local elements = Utils.imap(
   content,
   function(content_element)
      if type(content_element) == "string" then
         return content_element
      else
         return extract_element(content_element)
      end
   end)

   return Element.new(tag, attributes, _tl_table_unpack(elements))
end

function HTMTable.new(tree)
   return Utils.imap(tree, extract_element)
end

return HTMTable
