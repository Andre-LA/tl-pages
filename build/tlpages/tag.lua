local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table; local type = type




local Utils = require('tlpages.utils')

local Tag = {}




function Tag.attribute(attribute)
   if type(attribute) == "string" then
      return string.lower(attribute)
   else
      local name, value = attribute[1], attribute[2]
      if name and value then
         return (
         string.lower(name) ..
         '=' ..
         ((type(value) == "string") and '"' .. value .. '"' or tostring(value)))

      else
         return nil
      end
   end
end

function Tag.attributes(attributes)
   return table.concat(
   Utils.imap(attributes, Tag.attribute),
   ' ')

end

function Tag.start_tag(tag, attributes)
   local attribs = Tag.attributes(attributes or {})
   return '<' .. tag .. ((#attribs > 0) and ' ' .. attribs or '') .. '>'
end

function Tag.end_tag(tag)
   return '</' .. tag .. '>'
end

function Tag.render(tag, attributes, content)
   local has_content = content and content ~= ''
   return (
   Tag.start_tag(tag, attributes or {}) ..
   (content or '') ..
   (has_content and Tag.end_tag(tag) or ''))

end

return Tag
